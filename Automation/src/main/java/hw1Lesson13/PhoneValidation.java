package hw1Lesson13;

import java.util.Scanner;

public class PhoneValidation {
    public static void main(String[] args) {
        System.out.println("Enter valid number");
        validWithCondition();
    }


    private static void validWithCondition() {
        Scanner number = new Scanner(System.in);
        String phone = number.next();
        if (phone.length() >= 9 && phone.length() < 13 && isNumeric(phone)) {
            System.out.println("Valid phone number.");
            return;
        }
        System.out.println("Invalid number.");
        validWithCondition();
    }

    private static Boolean isNumeric(String data) {
        for (char aChar : data.toCharArray()) {
            if (!Character.isDigit(aChar)) {
                return false;
            }
        }
        return true;
    }
}
